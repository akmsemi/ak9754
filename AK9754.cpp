#include "AK9754.h"
#include "AK9754_reg.h"


AK9754::AK9754() {
}


void AK9754::init(I2C *conn, SlaveAddress addr) {
    slaveAddress = addr;
    connection = conn;
}


AK9754::Status AK9754::verifyDevice() {

	Status status = SUCCESS;
    char buf[2];

    status = read(AK9754_REG_WIA1, buf, 2);
    if (status != SUCCESS)	return status;

    // Compare received device values with expected WIA values
    if ( (buf[0] != AK9754_VAL_WIA1) || (buf[1] != AK9754_VAL_WIA2) ) {
        return ERROR;
    }

    return status;
}


AK9754::Status AK9754::getInterruptEnable(InterruptEnable* intEnable){

	Status status;
    char buf = 0;

    status = read(AK9754_REG_CNTL11, &buf, AK9754_LEN_REG_CNTL11);
    if(status != SUCCESS)	return status;

    intEnable->drien = ((buf & AK9754_MASK_CNTL11_DRIEN) > 0) ? true : false;
    intEnable->hbdien = ((buf & AK9754_MASK_CNTL11_HBDIEN) > 0) ? true : false;

    return SUCCESS;
}


AK9754::Status AK9754::setInterruptEnable(const AK9754::InterruptEnable* intEnable) {

    Status status;
    char buf = 0;

    status = read(AK9754_REG_CNTL11, &buf, AK9754_LEN_REG_CNTL11);
	if(status != SUCCESS)	return status;

    buf |= (intEnable->drien ? AK9754_MASK_CNTL11_DRIEN : 0);
    buf |= (intEnable->hbdien ? AK9754_MASK_CNTL11_HBDIEN : 0);

    status = write(AK9754_REG_CNTL11, &buf, AK9754_LEN_REG_CNTL11);
    if (status != SUCCESS)	return status;

    return SUCCESS;
}


AK9754::Status AK9754::getOperationMode(OperationMode* mode) {
	Status status;
    char buf;

    status = read(AK9754_REG_CNTL12, &buf, AK9754_LEN_REG_CNTL12);
	if (status != SUCCESS)	return status;

	*mode = (OperationMode)(buf & AK9754_MASK_CNTL12_MODE);

    return SUCCESS;
}


AK9754::Status AK9754::setOperationMode(OperationMode* mode) {
	Status status;
	char buf;

    buf = *mode;

    status = write(AK9754_REG_CNTL12, &buf, AK9754_LEN_REG_CNTL12);
	if (status != SUCCESS)	return status;

    return SUCCESS;
}


AK9754::Status AK9754::getOperationSettings(OpSettings* settings) {
	Status status;
	const int len = AK9754_REG_CNTL6 - AK9754_REG_CNTL3 + 1;
	char buf[len];

	status = read(AK9754_REG_CNTL3, buf, len);
	if (status != SUCCESS)	return status;

	settings->low_noise = ((buf[0] & AK9754_MASK_CNTL3_LNM) >> AK9754_OFFSET_CNTL3_LNM) ? true : false;
	settings->fcir = (DigitalFilter)(buf[0] & AK9754_MASK_CNTL3_FCIR);
	settings->fctmp = (DigitalFilter)((buf[0] & AK9754_MASK_CNTL3_FCTMP) >> AK9754_OFFSET_CNTL3_FCTMP);
	settings->odr = (ODR)((buf[0] & AK9754_MASK_CNTL3_ODR) >> AK9754_OFFSET_CNTL3_ODR);
	settings->opt = (Optimize)(buf[1] & AK9754_MASK_CNTL4_TOPT);
	settings->th_adj = ((buf[1] & AK9754_MASK_CNTL4_THADJ) >> AK9754_OFFSET_CNTL4_THADJ) ? true : false;
	settings->tmp_offset = buf[2] & AK9754_MASK_CNTL5_TMPOFS;
	settings->ir_gain = buf[3] & AK9754_MASK_CNTL6_IRGAIN;

	return SUCCESS;
}


AK9754::Status AK9754::setOperationSettings(OpSettings* settings) {
	Status status;
	const int len = AK9754_REG_CNTL6 - AK9754_REG_CNTL3 + 1;
	char buf[len];

	buf[0] = settings->low_noise ? (settings->low_noise << AK9754_OFFSET_CNTL3_LNM) : 0;
	buf[0] |= (char)(settings->fcir);
	buf[0] |= (char)(settings->fctmp) << AK9754_OFFSET_CNTL3_FCTMP;
	buf[0] |= (char)(settings->odr) << AK9754_OFFSET_CNTL3_ODR;
	buf[1] = (char)(settings->opt);
	buf[1] |= (char)(settings->th_adj);
	buf[2] = (char)(settings->tmp_offset);
	buf[3] = (char)(settings->ir_gain);

	status = write(AK9754_REG_CNTL3, buf, len);
	if (status != SUCCESS)	return status;

	return SUCCESS;
}


AK9754::Status AK9754::getHbdSettings(HbdSettings* hbd_settings) {

	Status status;
	char buf;

	// Read idle time and signal inversion
	status = read(AK9754_REG_CNTL7, &buf, AK9754_LEN_REG_CNTL7);
	if(status != SUCCESS)	return status;

	hbd_settings->hbd_idle = (HbdIdleTime)(buf & AK9754_MASK_CNTL7_IDLET);
    hbd_settings->hbd_invert = ((buf & AK9754_MASK_CNTL7_IRINV) >> AK9754_OFFSET_CNTL7_IRINV ) ? true : false;

    // Read detection time
	status = read(AK9754_REG_CNTL8, &buf, AK9754_LEN_REG_CNTL8);
	if(status != SUCCESS)	return status;

	hbd_settings->hbd_det_time = (buf & AK9754_MASK_CNTL8_DTCT);

    // Read HBD enable
	status = read(AK9754_REG_CNTL11, &buf, AK9754_LEN_REG_CNTL11);
	if(status != SUCCESS)	return status;

	hbd_settings->hbd_en = ((buf & AK9754_MASK_CNTL11_HBDEN) >> AK9754_OFFSET_CNTL11_HBDEN) ? true : false;

    return SUCCESS;
}


AK9754::Status AK9754::setHbdSettings(HbdSettings* hbd_settings) {

	Status status;
	char buf;
	InterruptEnable intEn;

	// Write idle time and signal inversion
	buf = hbd_settings->hbd_invert ? AK9754_MASK_CNTL7_IRINV : 0;
	buf |= (char)(hbd_settings->hbd_idle);

	status = write(AK9754_REG_CNTL7, &buf, AK9754_LEN_REG_CNTL7);
	if(status != SUCCESS)	return status;

	// Write detection time
	buf = hbd_settings->hbd_det_time;

	status = write(AK9754_REG_CNTL8, &buf, AK9754_LEN_REG_CNTL8);
	if(status != SUCCESS)	return status;

	// Save interrupt enables for restoring
	getInterruptEnable(&intEn);
	buf = (intEn.drien ? AK9754_MASK_CNTL11_DRIEN : 0);
	buf |= (intEn.hbdien ? AK9754_MASK_CNTL11_HBDIEN : 0);
	buf |= (hbd_settings->hbd_en ? AK9754_MASK_CNTL11_HBDEN : 0);

	status = write(AK9754_REG_CNTL11, &buf, AK9754_LEN_REG_CNTL11);
	if(status != SUCCESS)	return status;

	return SUCCESS;
}


AK9754::Status AK9754::getThreshold(Threshold *th) {

	Status status;
    char buf[2];

    status = read(AK9754_REG_CNTL9, buf, 2);
    if (status != SUCCESS)	return status;

    th->hbdthl = buf[0];
    th->hbdthh = buf[1] & AK9754_MASK_CNTL10_HBDTH;

    return SUCCESS;
}


AK9754::Status AK9754::setThreshold(const Threshold *th) {
    Status status;
    char buf[2];

    buf[0] = th->hbdthl;   // Upper byte
    buf[1] = th->hbdthh;   // Lower byte

    status = write(AK9754_REG_CNTL9, buf, 2);
    if (status != SUCCESS)	return status;

    return SUCCESS;
}


AK9754::Status AK9754::getSensorData(AK9754::SensorData *data) {

	Status status;
    char buf[AK9754_LEN_DATA];

    // Read IR, temp, etc.
    status = read(AK9754_REG_ST1, buf, AK9754_LEN_DATA);
    if (status != SUCCESS) return status;

    // Check if data is ready
    if((buf[0] & AK9754_MASK_ST1_DRDY) == 0){
        return ERROR;							// DRDY=0, data not ready
    }

    // Read interrupt statuses
    data->intStatus.drdy = ((buf[0] & AK9754_MASK_ST1_DRDY) > 0) ? true : false;
    data->intStatus.hbdr1 = ((buf[0] & AK9754_MASK_ST1_HBDR1) > 0) ? true : false;
    data->intStatus.hbdr2 = ((buf[5] & AK9754_MASK_ST2_HBDR2) > 0) ? true : false;

    // IR data
    data->ir = (int16_t)((buf[2] << 8) | buf[1]);

    // Temperature data
    data->temperature = (int16_t)((buf[4] << 8) | buf[3]);

    // Check for data overrun
    data->dor = ((buf[5] & AK9754_MASK_ST2_DOR) > 0) ? true : false;

    return SUCCESS;
}


AK9754::Status AK9754::isDataReady() {

	Status status;
    char stValue;

    status = read(AK9754_REG_ST1, &stValue, 1);
    if (status != SUCCESS)	return status;

    if ((stValue & AK9754_MASK_ST1_DRDY) > 0) {
        status = DATA_READY;
    } else {
        status = NOT_DATA_READY;
    }

    return status;
}


AK9754::Status AK9754::reset() {

	Status status = SUCCESS;
    char val = AK9754_VAL_SOFTWARE_RESET;

    status = write(AK9754_REG_CNTL1, &val, AK9754_LEN_REG_CNTL1);

    return status;
}


AK9754::Status AK9754::read(char registerAddress, char *buf, int length) {

	// Writes the register address
    if (connection->write((slaveAddress << 1), &registerAddress, 1) != 0) {
        return ERROR_I2C_WRITE;
    }

    // Reads register data
    if (connection->read((slaveAddress << 1), buf, length) != 0) {
        return ERROR_I2C_READ;
    }

    return SUCCESS;
}


AK9754::Status AK9754::write(char registerAddress, const char *buf, int length) {

	char data[AK9754_LEN_BUF_MAX];

    // Creates data to be sent.
    data[0] = registerAddress;
    for (int i=0; i < length; i++) {
        data[1+i] = buf[i];
    }

    // Writes data.
    if (connection->write((slaveAddress << 1), data, length + 1) != 0) {
        return ERROR_I2C_WRITE;
    }

    return SUCCESS;
}
