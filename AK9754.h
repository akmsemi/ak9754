#ifndef __AK9754_H__
#define __AK9754_H__

#include "mbed.h"


/**
 * This is a device driver of AK9754.
 *
 * @note AK9754 IR sensor device manufactured by AKM.
 * Example:
 * @code
 * #include "mbed.h"
 * #include "AK9754.h"
 *
 * #define I2C_SPEED_100KHZ    100000
 * #define I2C_SPEED_400KHZ    400000
 *
 * int main() {
 *     // Creates an instance of I2C
 *     I2C connection(I2C_SDA0, I2C_SCL0);
 *     connection.frequency(I2C_SPEED_100KHZ);
 *
 *     // TBD
 *
 *     while(true) {
 *              // TBD
 *         }
 *     }
 * }
 * @endcode
*/
class AK9754
{
public:
    /**
     * Enum for return status.
     */
    typedef enum {
        SUCCESS,					/**< Success */
        ERROR,						/**< Error */
        ERROR_I2C_WRITE,			/**< I2C write error */
        ERROR_I2C_READ,				/**< I2C read error */
        ERROR_ARG_OUT_OF_BOUNDS,	/**< An argument is out of bounds */
        DATA_READY,					/**< Data ready */
        NOT_DATA_READY,				/**< Data ready is not asserted. */
    } Status;

    /**
     * Enum for slave address of AK9754.
     */
    typedef enum {
    	SLAVE_ADDR_1 = 0x60,		/**< CAD0=0,  CAD1=0 */
		SLAVE_ADDR_2 = 0x61,		/**< CAD0=NC, CAD1=0 */
		SLAVE_ADDR_3 = 0x62,		/**< CAD0=1,  CAD1=0 */
		SLAVE_ADDR_4 = 0x64,		/**< CAD0=0,  CAD1=NC */
		SLAVE_ADDR_5 = 0x65,		/**< CAD0=NC, CAD1=NC */
		SLAVE_ADDR_6 = 0x66,		/**< CAD0=1,  CAD1=NC */
		SLAVE_ADDR_7 = 0x68,		/**< CAD0=0,  CAD1=1 */
		SLAVE_ADDR_8 = 0x69,		/**< CAD0=NC, CAD1=1 */
    } SlaveAddress;

    /**
     * Enum for operation mode of measurement.
     */
    typedef enum {
        MODE_STANDBY = 0x00,		/**< Standby mode */
        MODE_CONTINUOUS = 0x01		/**< Continuous measurement mode */
    } OperationMode;

    /**
     * Available output data rates
     */
    typedef enum {
    	ODR_1HZ = 0x00,				/**< ODR = 1 Hz */
    	ODR_2HZ = 0x01,				/**< ODR = 2 Hz */
    	ODR_10HZ = 0x02,			/**< ODR = 10 Hz */
    	ODR_50HZ = 0x03				/**< ODR = 50 Hz */
    } ODR;

    /**
     * Enum for digital filter setting.
     */
    typedef enum {
    	DF_NONE = 0x00,				/**< No Filter */
    	DF_0P9HZ = 0x01,			/**< Fc = 0.9 Hz */
        DF_0P445HZ = 0x02,			/**< Fc = 0.445 Hz */
    } DigitalFilter;

    typedef enum {
    	OPT_DISABLE = 0x00,			/**< Optimization enabled */
		OPT_ENABLE = 0x03			/**< Optimization disabled */
    } Optimize;

    typedef enum {
    	IDLE_0_SEC = 0x00,			/**< HBD idle time = 0 seconds */
		IDLE_5_SEC = 0x01,			/**< HBD idle time = 5 seconds */
		IDLE_10_SEC = 0x02,			/**< HBD idle time = 10 seconds */
		IDLE_30_SEC = 0x03,			/**< HBD idle time = 30 seconds */
		IDLE_300_SEC = 0x04			/**< HBD idle time = 5 minutes */
    } HbdIdleTime;

    /**
     * Interrupt statuses
     */
    struct InterruptStatus {
        bool drdy;					/**< DRDY interrupt */
        bool hbdr1;					/**< Human Approach Detection interrupt 1 */
        bool hbdr2;					/**< Human Approach Detection interrupt 2 */
    };

    struct InterruptEnable {
    	bool drien;					/**< Data ready interrupt enable */
    	bool hbdien;				/**< Human presence interrupt enable */
    };

    /**
     * Structure for threshold.
     */
    struct Threshold {
        int8_t hbdthl;				/**< HBD threshold lower byte */
        int8_t hbdthh;				/**< HBD threshold upper byte */
    };

    /**
     * Device operation settings
     */
    struct OpSettings {
    	DigitalFilter fcir;			/**< IR filter cutoff frequency */
    	DigitalFilter fctmp;		/**< Temperature filter cutoff frequency */
    	ODR odr;					/**< Output data rate */
    	bool low_noise;				/**< Low noise mode enable */
    	Optimize opt;				/**< Optimization enable */
    	bool th_adj;				/**< Threshold adjust enable */
    	uint8_t tmp_offset;			/**< Temperature offset value */
    	uint8_t ir_gain;			/**< IR gain value */
    };

    /**
     * Human presence detection settings
     */
    struct HbdSettings {
    	bool hbd_en;				/**< Human Presence Detection enable */
    	bool hbd_invert;			/**< HBD Algorithm Input Signal Invert */
		HbdIdleTime hbd_idle;		/**< HBD Pre-Threshold Idle Time */
		uint8_t hbd_det_time;		/**< HBD Detection Time Period */
		//bool sb_en;					/**< Streaming Buffer enable */
		//bool sb_hbd;				/**< Update Streaming Buffer by HBD */
    };

    /**
     * Structure for measurement data.
     */
    struct SensorData {
        InterruptStatus intStatus;	/**< Interrupt status */
        int16_t ir;					/**< IR sensor data */
        int16_t temperature;		/**< Temperature sensor data */
        bool dor;					/**< Data over run status */
    };

    /**
     * Constructor.
     *
     */
    AK9754();

    /**
     * Establishes the I2C connection based on the provided slave address.
     *
     * @param conn instance of I2C
     * @param addr slave address of the device
     */
    void init(I2C *conn, SlaveAddress addr);


    /**
     * Verifies the AK9754 by checking the WIA values.
     *
     * @return Returns SUCCESS if connection is confirmed, otherwise returns other value.
     */
    Status verifyDevice();


    /**
     * Gets interrupt enable/disable status.
     *
     * @param intStatus interrupt status
     * @return SUCCESS if the interrupt status is obtained successfully, otherwise returns other value.
     */
    Status getInterruptEnable(InterruptEnable* intEnable);


    /**
     * Sets interrupt enable/disable status.
     *
     * @param intStatus interrupt status
     * @return SUCCESS if the interrupt status is set successfully, otherwise returns other value.
     */
    Status setInterruptEnable(const InterruptEnable* intEnable);


    /**
     * Gets sensor operation mode.
     *
     * @param mode Pointer to the operation mode.
     * @param filter Pointer to the digital filter setting.
     * @return SUCCESS if operation mode is set successfully, otherwise returns other value.
     */
    Status getOperationMode(OperationMode* mode);


    /**
     * Sets sensor operation mode.
     *
     * @param mode operation mode to be set
     * @return SUCCESS if operation mode is set successfully, otherwise returns other value
     */
    Status setOperationMode(OperationMode* mode);


    /**
     * Retrieve the operational settings of the sensor
     *
     * @param settings Reference to OpSettings structure
     * @return SUCCESS if operation mode is set successfully, otherwise returns other value
     */
    Status getOperationSettings(OpSettings* settings);


    /**
     * Set the operational settings of the sensor
     *
     * @param settings Reference to OpSettings structure
     * @return SUCCESS if operation mode is set successfully, otherwise returns other value
     */
    Status setOperationSettings(OpSettings* settings);


    /**
     * Gets the Human Presence Detection settings.
     */
    Status getHbdSettings(HbdSettings* hbd_settings);


    /**
     * Sets the Human Presence Detection settings.
     */
    Status setHbdSettings(HbdSettings* hbd_settings);

    /**
     * Gets threshold.
     *
     * @param th Pointer to the threshold structure to store the read data.
     * @return SUCCESS if threshold is read successfully, otherwise returns other value.
     */
    Status getThreshold(Threshold *th);


    /**
     * Sets threshold.
     *
     * @param th Pointer to the threshold structure to be set.
     * @return SUCCESS if threshold is set successfully, otherwise returns other value.
     */
    Status setThreshold(const Threshold *th);


    /**
     * Gets all sensor data.
     *
     * @param data Address of compound data structure
     * @return SUCCESS if data retrieved successfully, failure code otherwise
     */
    Status getSensorData(SensorData *data);


    /**
     * Check if data is ready, i.e. measurement is finished.
     *
     * @return Returns DATA_READY if data is ready or NOT_DATA_READY if data is not ready. If error happens, returns another code.
     */
    Status isDataReady();


    /**
     * Soft reset.
     *
     * @return SUCCESS if reset successfully, failure code otherwise
     */
    Status reset();


    /**
     * Reads register(s).
     * @param registerAddress Register address to be read.
     * @param buf Buffer to store the read data.
     * @param length Length in bytes to be read.
     * @return SUCCESS if data is read successfully, otherwise returns other value.
     */
    Status read(char registerAddress, char *buf, int length);


    /**
     * Writes data into register(s).
     * @param registerAddress Register address to be written.
     * @param buf Data to be written.
     * @param length Length in bytes to be written.
     * @return SUCCESS if data is written successfully, otherwise returns other value.
     */
    Status write(char registerAddress, const char *buf, int length);

private:
    I2C *connection;				/**< I2C connection inherited from akmsensormanager */
    SlaveAddress slaveAddress;		/**< Slave address */
};

#endif // __AK9754_H__
