#ifndef __AK9754_REG_H__
#define __AK9754_REG_H__

// Values
#define AK9754_VAL_WIA1					0x48
#define AK9754_VAL_WIA2					0x15
#define AK9754_VAL_SOFTWARE_RESET		0x01

// Register Map
#define AK9754_REG_WIA1					0x00
#define AK9754_REG_WIA2					0x01
#define AK9754_REG_INFO1				0x02
#define AK9754_REG_INFO2				0x03
#define AK9754_REG_ST1					0x04
#define AK9754_REG_IRL					0x05
#define AK9754_REG_IRH					0x06
#define AK9754_REG_TMPL					0x07
#define AK9754_REG_TMPH					0x08
#define AK9754_REG_ST2					0x09
#define AK9754_REG_ST3					0x0A
#define AK9754_REG_SB0L					0x0B
#define AK9754_REG_SB0H					0x0C
#define AK9754_REG_SB1L					0x0D
#define AK9754_REG_SB1H					0x0E
#define AK9754_REG_SB2L					0x0F
#define AK9754_REG_SB2H					0x10
#define AK9754_REG_SB3L					0x11
#define AK9754_REG_SB3H					0x12
#define AK9754_REG_SB4L					0x13
#define AK9754_REG_SB4H					0x14
#define AK9754_REG_SB5L					0x15
#define AK9754_REG_SB5H					0x16
#define AK9754_REG_SB6L					0x17
#define AK9754_REG_SB6H					0x18
#define AK9754_REG_SB7L					0x19
#define AK9754_REG_SB7H					0x1A
#define AK9754_REG_SB8L					0x1B
#define AK9754_REG_SB8H					0x1C
#define AK9754_REG_SB9L					0x1D
#define AK9754_REG_SB9H					0x1E
#define AK9754_REG_ST4					0x1F
#define AK9754_REG_CNTL1				0x20
#define AK9754_REG_CNTL2				0x21
#define AK9754_REG_CNTL3				0x22
#define AK9754_REG_CNTL4				0x23
#define AK9754_REG_CNTL5				0x24
#define AK9754_REG_CNTL6				0x25
#define AK9754_REG_CNTL7				0x26
#define AK9754_REG_CNTL8				0x27
#define AK9754_REG_CNTL9				0x28
#define AK9754_REG_CNTL10				0x29
#define AK9754_REG_CNTL11				0x2A
#define AK9754_REG_CNTL12				0x2B

// Masks
#define AK9754_MASK_ST1_DRDY			0x01
#define AK9754_MASK_ST1_HBDR1			0x10
#define AK9754_MASK_ST2_DOR				0x01
#define AK9754_MASK_ST2_HBDR2			0x10
#define AK9754_MASK_CNTL3_FCIR			0x03
#define AK9754_MASK_CNTL3_FCTMP			0x0C
#define AK9754_MASK_CNTL3_ODR			0x30
#define AK9754_MASK_CNTL3_LNM			0x40
#define AK9754_MASK_CNTL4_TOPT			0x03
#define AK9754_MASK_CNTL4_THADJ			0x04
#define AK9754_MASK_CNTL5_TMPOFS		0x7F
#define AK9754_MASK_CNTL6_IRGAIN		0x1F
#define AK9754_MASK_CNTL7_IRINV			0x08
#define AK9754_MASK_CNTL7_IDLET			0x07
#define AK9754_MASK_CNTL8_DTCT			0x7F
#define AK9754_MASK_CNTL10_HBDTH		0x7F
#define AK9754_MASK_CNTL11_DRIEN		0x01
#define AK9754_MASK_CNTL11_HBDIEN		0x02
#define AK9754_MASK_CNTL11_SBEN			0x04
#define AK9754_MASK_CNTL11_SBHBD		0x08
#define AK9754_MASK_CNTL11_HBDEN		0x10
#define AK9754_MASK_CNTL12_MODE			0x01

// Offsets
#define AK9754_OFFSET_CNTL3_LNM			6
#define AK9754_OFFSET_CNTL3_ODR			4
#define AK9754_OFFSET_CNTL3_FCTMP		2
#define AK9754_OFFSET_CNTL4_THADJ		2
#define AK9754_OFFSET_CNTL7_IRINV		3
#define AK9754_OFFSET_CNTL11_HBDEN		4
#define AK9754_OFFSET_CNTL11_SBHBD		3
#define AK9754_OFFSET_CNTL11_SBEN		2
#define AK9754_OFFSET_CNTL11_HBDIEN		1

// Assorted Lengths
#define AK9754_LEN_STR_BUF				10
#define AK9754_LEN_DATA					6
#define AK9754_LEN_IR_DATA				2
#define AK9754_LEN_TMP_DATA				2
#define AK9754_LEN_BUF_THRESHOLD		8
#define AK9754_LEN_BUF_HYSTERESIS		2
#define AK9754_LEN_BUF_MAX				7

// Register Lengths
#define AK9754_LEN_REG_WIA1				1
#define AK9754_LEN_REG_WIA2				1
#define AK9754_LEN_REG_INFO1			1
#define AK9754_LEN_REG_INFO2			1
#define AK9754_LEN_REG_ST1				1
#define AK9754_LEN_REG_IRL				1
#define AK9754_LEN_REG_IRH				1
#define AK9754_LEN_REG_TMPL				1
#define AK9754_LEN_REG_TMPH				1
#define AK9754_LEN_REG_ST2				1
#define AK9754_LEN_REG_ST3				1
#define AK9754_LEN_REG_SB0L				1
#define AK9754_LEN_REG_SB0H				1
#define AK9754_LEN_REG_SB1L				1
#define AK9754_LEN_REG_SB1H				1
#define AK9754_LEN_REG_SB2L				1
#define AK9754_LEN_REG_SB2H				1
#define AK9754_LEN_REG_SB3L				1
#define AK9754_LEN_REG_SB3H				1
#define AK9754_LEN_REG_SB4L				1
#define AK9754_LEN_REG_SB4H				1
#define AK9754_LEN_REG_SB5L				1
#define AK9754_LEN_REG_SB5H				1
#define AK9754_LEN_REG_SB6L				1
#define AK9754_LEN_REG_SB6H				1
#define AK9754_LEN_REG_SB7L				1
#define AK9754_LEN_REG_SB7H				1
#define AK9754_LEN_REG_SB8L				1
#define AK9754_LEN_REG_SB8H				1
#define AK9754_LEN_REG_SB9L				1
#define AK9754_LEN_REG_SB9H				1
#define AK9754_LEN_REG_ST4				1
#define AK9754_LEN_REG_CNTL1			1
#define AK9754_LEN_REG_CNTL2			1
#define AK9754_LEN_REG_CNTL3			1
#define AK9754_LEN_REG_CNTL4			1
#define AK9754_LEN_REG_CNTL5			1
#define AK9754_LEN_REG_CNTL6			1
#define AK9754_LEN_REG_CNTL7			1
#define AK9754_LEN_REG_CNTL8			1
#define AK9754_LEN_REG_CNTL9			1
#define AK9754_LEN_REG_CNTL10			1
#define AK9754_LEN_REG_CNTL11			1
#define AK9754_LEN_REG_CNTL12			1

#endif // __AK9754_REG_H__
